# DU_Display_ShipContainerCapacity

## Version 2.0 Now Released!
Thank you to Harleen Xolkyr for your awesome contributions in helping to paramaterize the script. Also added the ability to give AGG warnings. You can set a threshold and custom message as to when to enable your ship's AGG.

A basic LUA script for Dual Universe. Connects a Programming Board, a screen, and either a Container or a Container HUB to show current weight. Includes customizable features such as colors based on thresholds.

![Container Hub Preview](images/containerHub_medium.PNG)
![Container Hub Parameters](images/luaParameters.png)
