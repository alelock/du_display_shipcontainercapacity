-- Ship Container Load Progress Bar
-- IGN: Alelock
-- Special thanks to other contributor(s): Harleen Xolkiyr
-- version 2.0
-- Release as part of DU Open Source Initiative
-- Discord: https://discord.gg/SuypjxnDy4

---------------------------------------------------------------
-- READ THIS!!!!
---------------------------------------------------------------
-- SETUP
-- Connect screen and name it 'screen'
-- Connect container (or conatiner Hub) and name it 'largeCont'
-- Customize maxLoad, topMargin, highVal, lowVal, highValColor, midValColor, and lowValColor


-- Paste all of this in a programming board -> unit -> Add Filter -> start()
-- Paste the next line in programming board -> unit -> Add Filter -> tick() with the name 'refreshScreen'
	-- refreshScreen()

-- Set these values for your ship.
maxLoad = 850 --export: What do you want your max load to be (in Tons)
topMargin = 20 --export: Play with this number to move your visual vertically on the screen.
			-- 0 will be top justified
			-- 100 will put is at the very bottom.
			-- It's technically a % value


-- Set Weight Bounds Percentage
highVal = 75 --export: OVER this % will be highValColor
lowVal = 50 --export: UNDER this % will be lowValColor
		  -- In between will take midValColor

-- Set colors
 
highValColor = "ED0003" --export HEX Color Value for HIGH tonage. Omit the #
midValColor = "FFF08D" --export HEX Color Value for MID tonage. Omit the #
lowValColor = "9BC693" --export HEX Color Value for LOW tonage. Omit the #


-- AGG Settings:
enableAGGWarning = true --export: Toggle whether AGG warning appears or not
AGGWeight = 100 --export: Weight, in tonnes, to show AGG warning
AGGTextColor = "ED0003" --export: AGG Warning Text HEX Color. Omit the #
AGGText = "Enable AGG" --export: Agg Warning Text

-- Now the code
maxChartWidth = 1890

function getContainer()
    return largeCont.getItemsMass()
end

function round(number,decimals)
    local power = 10^decimals
    return math.floor((number) * power) / power
end 

function getLoadPercent(contLoad_Rnd)
	return round(((contLoad/1000)/maxLoad) * 100,2)

end

function getChartFilledWidth(contPercent)
    pecentToDecimal = contPercent/100
    return maxChartWidth*pecentToDecimal
end

function getFillColor(contPercent)
   if (contPercent >= 75) then
        return highValColor
   elseif (contPercent < 50) then
        return lowValColor
   else
        return midValColor
   end
end

function refreshScreen()
    contLoad = getContainer()
    contLoad_Rnd = round(contLoad/1000,2)
    contPercent = getLoadPercent(contLoad_Rnd)
    cartFilledWidth = getChartFilledWidth(contPercent)
    --screen.setCenteredText(contLoad_Rnd .. " t" .. " / " .. contPercent);
    
    if (contLoad_Rnd >= AGGWeight) and (enableAGGWarning == true) then
        AGGTextout = AGGText
    else
        AGGTextout = ""
    end
    
    screen.setHTML(
        [[
        <style>
        	.top{margin-top: ]].. topMargin ..[[%;}
        	.aggWarning{width: 100%; margin: 0 auto;}
         </style>
        <div class="top">
            <svg class="bootstrap" viewBox="0 0 1920 1080" style="width:100%; height:100%">
            <svg class="bootstrap" viewBox="0 0 1920 1120" style="width:100%; height:100%">
            <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg">
        		<text x="15" y="120.0" font-size="80" text-anchor="start" fill="orange">Container Mass: ]] .. contLoad_Rnd .. [[t</text>
        		<rect x="15" y="135.0" width="]]..cartFilledWidth ..[[" height="190.0" fill="#]].. getFillColor(contPercent) ..[[" stroke-width="0"/>
                  <rect x="15" y="135.0" width="]] .. maxChartWidth .. [[" height="190.0" fill="#2f343d" fill-opacity="0" stroke="white" stroke-width="5"/>
                  <text x="1518.2"y="242.5" font-size="60" text-anchor="start" fill="white">]] .. contPercent .. [[%</text>
            </svg>
            </svg>
            </svg>
        

        </div>
        <div class="aggWarning">
        	<p style="text-align:center; font-size: 10vw; color:#]].. AGGTextColor .. [[">]] .. AGGTextout .. [[</p>
        </div>
        ]]
    );
    
end
unit.hide()
refreshScreen()
unit.setTimer("refreshScreen",1)


